FROM node:lts-alpine3.18
WORKDIR /home/app
COPY . /home/app
RUN npm install
CMD ["npm", "start"]
